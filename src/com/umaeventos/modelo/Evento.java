package com.umaeventos.modelo;

import java.io.Serializable;
import java.util.Date;

public class Evento implements Serializable {
	
	  /**
	 * 
	 */
	  private static final long serialVersionUID = 119501698274550663L;
	  
	  private Integer idevento;
	  private String titulo;
	  private String descripcion;
	  private Date fechacomienzo;
	  private Date fechafinal;
	  private String tipoevento;
	  private Integer plazas;
	  private Sede sede;
	
	  public Evento() {
		
	  }

	public Evento(Integer idevento, String titulo, String descripcion,
			Date fechacomienzo, Date fechafinal, String tipoevento,
			Integer plazas, Sede idsede) {
		this.idevento = idevento;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.fechacomienzo = fechacomienzo;
		this.fechafinal = fechafinal;
		this.tipoevento = tipoevento;
		this.plazas = plazas;
		this.sede = idsede;
	}

	public Integer getIdevento() {
		return idevento;
	}

	public void setIdevento(Integer idevento) {
		this.idevento = idevento;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechacomienzo() {
		return fechacomienzo;
	}

	public void setFechacomienzo(Date fechacomienzo) {
		this.fechacomienzo = fechacomienzo;
	}

	public Date getFechafinal() {
		return fechafinal;
	}

	public void setFechafinal(Date fechafinal) {
		this.fechafinal = fechafinal;
	}

	public String getTipoevento() {
		return tipoevento;
	}

	public void setTipoevento(String tipoevento) {
		this.tipoevento = tipoevento;
	}

	public Integer getPlazas() {
		return plazas;
	}

	public void setPlazas(Integer plazas) {
		this.plazas = plazas;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}


	
	
}
