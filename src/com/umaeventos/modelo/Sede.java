package com.umaeventos.modelo;

import java.io.Serializable;

public class Sede implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer idsede;
	private String nombre;
	private String direccion;

	
	
	public Sede() {
		
	}

	public Sede(Integer idsede, String nombre, String direccion) {
		
		this.idsede = idsede;
		this.nombre = nombre;
		this.direccion = direccion;
	}

	
	
	
	public Integer getIdsede() {
		return idsede;
	}

	public void setIdsede(Integer idsede) {
		this.idsede = idsede;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Sede [nombre=" + nombre + "]";
	}
	
	

}
