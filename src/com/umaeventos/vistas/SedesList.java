package com.umaeventos.vistas;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.umaeventos.R;
import com.umaeventos.modelo.Evento;
import com.umaeventos.modelo.Sede;
import com.umaeventos.utils.SedesAdapter;
import com.umaeventos.utils.ServiceHandler;

@SuppressLint("ValidFragment")
public class SedesList extends ListFragment {
	
	private SedesAdapter adapter;
	private List<Sede> sedes;
	private ProgressDialog pdialog;
	public SedesList(){
		
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
        sedes = new ArrayList<Sede>();
        new GetContacts().execute();
        
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list_fragment, container, false);
	}
	
	@Override
	public void onListItemClick(ListView list, View v, int position, long id) {
		Sede sede = (Sede)getListView().getItemAtPosition(position);
		
		//EventsList listallevents = new EventsList(sede.getIdsede());
		//getFragmentManager().beginTransaction().replace(R.id.newfragment, listallevents).commit();  

		
        Intent i = new Intent(getActivity(), EventsActivity.class);
        i.putExtra("idsede", sede.getIdsede());
        i.putExtra("nombresede", sede.getNombre());
        startActivity(i);
    }
	
	 /**
     * Async task class to get json by making HTTP call
     * */
    private class GetContacts extends AsyncTask<Void, Void, Void> {
 
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pdialog = ProgressDialog.show(getActivity(), "", "Cargando...");
            pdialog.setCancelable(false);
            
        }
 
        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
        	
            ServiceHandler sh = new ServiceHandler();
            sedes = sh.getSedes();
        	
            return null;
            
        }
 
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
    		adapter = new SedesAdapter(getActivity(), sedes);
    		setListAdapter(adapter);
	   		if(pdialog.isShowing())
	   			pdialog.dismiss();
    		
        }
 
    }
}



