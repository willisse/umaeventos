package com.umaeventos.vistas;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.umaeventos.R;
import com.umaeventos.modelo.Evento;
import com.umaeventos.utils.EventosAdapter;
import com.umaeventos.utils.ServiceHandler;
import com.umaeventos.vistas.EventActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

@SuppressLint("ValidFragment")
public class LastEventsList extends ListFragment {
	private List<Evento> eventos;
	private EventosAdapter adapter;
	private ProgressDialog pdialog;
	
	public LastEventsList(){
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        eventos = new ArrayList<Evento>();
        new GetProximosEventos().execute();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list_fragment, container, false);
	}
	
	@Override
	public void onListItemClick(ListView list, View v, int position, long id) {
        // selected item 
		Evento evento = (Evento)getListView().getItemAtPosition(position);
        
        // Nueva actividad, pasando evento
        Intent i = new Intent(getActivity(), EventActivity.class);
        i.putExtra("eventosede", (Serializable)evento);
        startActivity(i);
	}
	

	 /**
    * Async task class to get json by making HTTP call
    * */
   private class GetProximosEventos extends AsyncTask<Void, Void, Void> {

       @Override
       protected void onPreExecute() {
           super.onPreExecute();
           
           pdialog = ProgressDialog.show(getActivity(), "", "Cargando...");
           pdialog.setCancelable(false);
       }

       @Override
       protected Void doInBackground(Void... arg0) {
           // Creating service handler class instance
       	
           ServiceHandler sh = new ServiceHandler();
           try {
			eventos = sh.getProximosEventos();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       	
           return null;
           
       }

       @Override
       protected void onPostExecute(Void result) {
           super.onPostExecute(result);
	   		adapter = new EventosAdapter(getActivity(), eventos);
	   		setListAdapter(adapter);      
	   		
	   		if(pdialog.isShowing())
	   			pdialog.dismiss();
       }

   }	
}



