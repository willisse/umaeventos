package com.umaeventos.vistas;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract.Events;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;

import com.umaeventos.R;
import com.umaeventos.modelo.Evento;

class MyCalendar {
	public String name;
	public String id;
	public MyCalendar(String _name, String _id) {
		name = _name;
		id = _id;
	}
	@Override
	public String toString() {
		return name;
	}
}

public class LoginActivity extends Activity {

	private static final String LOGTAG = "MAPSGOOGLE";
	
	private Evento evento;
	
	private Spinner m_spinner_calender;
	//private MyCalendar m_calendars[];
    private String m_selectedCalendarId = "0";
	
    /** Called when the user clicks the Send button */
    public void enter(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		Intent i = getIntent();
		evento = (Evento) i.getSerializableExtra("evento");//Recoge evento
		
		getCalendars();
        //populateCalendarSpinner();
		addEvent();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/*add an event to calendar*/
    @SuppressLint("NewApi")
	private void addEvent() {
    	ContentResolver cr = this.getContentResolver();
    	ContentValues l_event = new ContentValues();
    	l_event.put("calendar_id", m_selectedCalendarId);
    	l_event.put("title", evento.getTitulo());
    	l_event.put("description", evento.getDescripcion());
    	l_event.put("eventLocation", evento.getSede().getDireccion());
    	//l_event.put("duration", "PT"+evento.getEvento().getHoras()+"H");
    	l_event.put(Events.EVENT_TIMEZONE, "España/Mñalaga");
    	l_event.put("dtstart", System.currentTimeMillis() + 7000*1000);
    	l_event.put("dtend", System.currentTimeMillis() + 100*1000);
    	l_event.put("allDay", 0);
    	//status: 0~ tentative; 1~ confirmed; 2~ canceled
    	l_event.put("eventStatus", 1);
    	//0~ false; 1~ true
    	l_event.put("hasAlarm", 1);
    	Uri l_eventUri;
    	if (Build.VERSION.SDK_INT >= 8) {
    		l_eventUri = Uri.parse("content://com.android.calendar/events");
    	} else {
    		l_eventUri = Uri.parse("content://calendar/events");
    	}
    	Uri l_uri = cr.insert(l_eventUri, l_event);
    	
    	Log.i(LOGTAG, l_uri.toString());

    	
    }
    
    /*private void populateCalendarSpinner() {
    	m_spinner_calender = (Spinner)this.findViewById(R.id.spinner_calendar);
    	ArrayAdapter l_arrayAdapter = new ArrayAdapter(this.getApplicationContext(), android.R.layout.simple_spinner_item, m_calendars);
    	l_arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	m_spinner_calender.setAdapter(l_arrayAdapter);
    	m_spinner_calender.setSelection(0);
    	m_spinner_calender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> p_parent, View p_view,
					int p_pos, long p_id) {
				m_selectedCalendarId = m_calendars[(int)p_id].id;
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {}
		});
    }*/
    
    public void getCalendars() {

    	String projection[] = {"_id", "name"};
	    Uri calendars;
	    calendars = Uri.parse("content://com.android.calendar/calendars");
	    String result = null;

	    @SuppressWarnings("deprecation")
		Cursor managedCursor = this.managedQuery(calendars, projection, null, null, null);
	    
	    if (managedCursor != null && managedCursor.moveToFirst()) {


            int nameColumn = managedCursor.getColumnIndex("name");
            int idColumn = managedCursor.getColumnIndex("_id");

            do {
                String calName = managedCursor.getString(nameColumn);
                String calId = managedCursor.getString(idColumn);

                // You have to give email id in below line, right now i puted my email id
                if (calName != null &&( calName.equals("victorcg88@gmail.com") || calName.equals("Google Calendar"))) {
                    result = calId;
                }
            } while (managedCursor.moveToNext());
        } else {
        }
	    m_selectedCalendarId=result;

	}

}
