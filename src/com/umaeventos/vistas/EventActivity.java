package com.umaeventos.vistas;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.umaeventos.R;
import com.umaeventos.modelo.Evento;

public class EventActivity extends Activity{
	
	private static final String LOGTAG = "MAPSGOOGLE";
	
	private GoogleMap map;
	
	Geocoder geocoder = new Geocoder(this);
	
	//private double lat = 36.721682;
    //private double lon = -4.485174400000005;
    private double lat, lon;
    
    private Evento evento;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_event);
		
		TextView tv_nombre = (TextView) findViewById(R.id.nombre_label);
		TextView tv_descripcion = (TextView) findViewById(R.id.descripcion_label);
		TextView tv_fecha = (TextView) findViewById(R.id.fecha_label);
		TextView tv_hora = (TextView) findViewById(R.id.hora_label);
		TextView tv_tipo = (TextView) findViewById(R.id.tipo_label);
		TextView tv_sede = (TextView) findViewById(R.id.sede_label);
		TextView tv_direccion = (TextView) findViewById(R.id.direccion_label);
		
		Intent i = getIntent();
		
		evento = (Evento) i.getSerializableExtra("eventosede");//Recoge evento
		
		//Mostramos los datos en las vistas
		tv_nombre.setText(evento.getTitulo());
		tv_descripcion.setText(evento.getDescripcion());
		
		java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
		tv_fecha.setText("Fecha: " + dateFormat.format(evento.getFechacomienzo()));
		
		tv_hora.setText("Plazas: " + evento.getPlazas());
		tv_tipo.setText(evento.getTipoevento());
		tv_sede.setText(evento.getSede().getNombre());
		tv_direccion.setText(evento.getSede().getDireccion());
		
		//Creamos y añadimos el mapa
		getCoordinates(evento.getSede().getDireccion());
		
		setUpMapIfNeeded(lat, lon);
		MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lon)).title(evento.getTitulo()).snippet(evento.getDescripcion());
		
		map.addMarker(marker);
	}
	
	public void asistir(View view){
		Intent intent = new Intent(this, LoginActivity.class);
		intent.putExtra("evento", evento);
		startActivity(intent);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void getCoordinates (String direccion){
		try {
			List <Address> sitios = geocoder.getFromLocationName(direccion, 1);
			
			if(sitios.size() > 0){
				
				lat = sitios.get(0).getLatitude();
				lon = sitios.get(0).getLongitude();
			}
			else{
				Toast.makeText(this, "Introduce una direccion valida", Toast.LENGTH_SHORT).show();
				Toast.makeText(this, "Introduce una direccion válida", Toast.LENGTH_SHORT).show();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setUpMapIfNeeded(Double lat, Double lon) {
		
		if (map == null) {
			map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
			
			// check if map is created successfully or not
            if (map == null) {
                Toast.makeText(getApplicationContext(), "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
            }
            else{

            	//Map Options
        		map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        		//map.setMyLocationEnabled(true);
        		//map.getUiSettings().setMyLocationButtonEnabled(true);
        		map.getUiSettings().setZoomControlsEnabled(true);
        		map.getUiSettings().setZoomGesturesEnabled(true);
        		map.getUiSettings().setTiltGesturesEnabled(true);
        		map.getUiSettings().setRotateGesturesEnabled(true);
        		map.getUiSettings().setScrollGesturesEnabled(true);
        		map.getUiSettings().setCompassEnabled(false);
        		map.setPadding(0, 0, 0, 0);
        		
        		//Move Camera
        		CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lat, lon)).zoom(15).build();
        		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
		}
	}
	
	
}


