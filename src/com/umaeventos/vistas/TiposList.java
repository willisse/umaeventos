package com.umaeventos.vistas;

import java.util.ArrayList;

import com.umaeventos.R;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

@SuppressLint("ValidFragment")
public class TiposList extends ListFragment {
	
	private ArrayList<String> tipos;
	
	public TiposList(){
		tipos = new ArrayList<String>();
		
		tipos.add("Charla");
		tipos.add("Conferencia");
		tipos.add("Seminario");
		tipos.add("Graduación");
		tipos.add("Taller");
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ListAdapter listAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, tipos);
		setListAdapter(listAdapter);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list_fragment, container, false);
	}
	
	@Override
	public void onListItemClick(ListView list, View v, int position, long id) {
		String tipo = (String)getListView().getItemAtPosition(position);

        Intent i = new Intent(getActivity(), EventsActivity.class);
        i.putExtra("tipo", tipo);
        startActivity(i);		
	}
}
