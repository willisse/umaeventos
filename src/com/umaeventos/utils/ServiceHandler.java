package com.umaeventos.utils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.umaeventos.modelo.Evento;
import com.umaeventos.modelo.Sede;
public class ServiceHandler {

	static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;
    private static final String URLWB = "http://192.168.1.116:8080/servidorumaeventos/webresources/";
 
    private List<Sede> sedes;
    private List<Evento> eventos;
    
    public ServiceHandler() {
 
    }
 
    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * */
    public String makeServiceCall(String url, int method) {
        return this.makeServiceCall(url, method, null);
    }
 
    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     * */
    public String makeServiceCall(String url, int method,
            List<NameValuePair> params) {
        try {
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;
             
            // Checking http request method type
            if (method == POST) {
                HttpPost httpPost = new HttpPost(url);
                // adding post params
                if (params != null) {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                }
 
                httpResponse = httpClient.execute(httpPost);
 
            } else if (method == GET) {
                // appending params to url
                if (params != null) {
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += "?" + paramString;
                }
                HttpGet httpGet = new HttpGet(url);
 
                httpResponse = httpClient.execute(httpGet);
 
            }
            httpEntity = httpResponse.getEntity();
            response = EntityUtils.toString(httpEntity);
 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
         
        return response;
 
    }

    public List<Evento> getEventosSede(Integer idsede) throws ParseException{
    	eventos = new ArrayList<Evento>();
		// Making a request to url and getting response
		sedes = new ArrayList<Sede>();
		String url_eventossede = URLWB + "com.eventos.jpa.evento"+ "/sede/" + idsede;
        String jsonStr = makeServiceCall(url_eventossede, ServiceHandler.GET);

        Log.d("Response: ", "> " + jsonStr);

        if (jsonStr != null) {
            try {
                JSONArray jsonObj = new JSONArray(jsonStr);
                for (int i = 0; i < jsonObj.length(); i++) {
                	JSONObject jso = jsonObj.getJSONObject(i);
                	Log.d("Response: ", "> " +jsonObj.toString());
                	
                	String fini = jso.getString("fechacomienzo");
                	String ffin = jso.getString("fechafinal");
                	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                	
                	JSONObject jsoS = jso.optJSONObject("idsede");
            		Sede s = new Sede(jsoS.getInt("idsede"), jsoS.getString("nombre"), jsoS.getString("direccion"));
                	Evento e =  new Evento(jso.optInt("idevento"),jso.optString("titulo"), jso.optString("descripcion"), sdf.parse(fini), sdf.parse(ffin), jso.optString("tipoevento"), jso.optInt("plazas"), s);
                	eventos.add(e); 
				}
               } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("ServiceHandler", "Couldn't get any data from the url");
        }

    	return eventos;
    }   
    
    public List<Evento> getEventosTipo(String tipo) throws ParseException{
    	eventos = new ArrayList<Evento>();
    	    	
		// Making a request to url and getting response
		sedes = new ArrayList<Sede>();
		String url_eventostipo = URLWB + "com.eventos.jpa.evento"+ "/tipo/" + tipo;
        String jsonStr = makeServiceCall(url_eventostipo, ServiceHandler.GET);

        Log.d("Response: ", "> " + jsonStr);

        if (jsonStr != null) {
            try {
                JSONArray jsonObj = new JSONArray(jsonStr);
                for (int i = 0; i < jsonObj.length(); i++) {
                	JSONObject jso = jsonObj.getJSONObject(i);
                	Log.d("Response: ", "> " +jsonObj.toString());
                	
                	String fini = jso.getString("fechacomienzo");
                	String ffin = jso.getString("fechafinal");
                	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                	
                	JSONObject jsoS = jso.optJSONObject("idsede");
            		Sede s = new Sede(jsoS.getInt("idsede"), jsoS.getString("nombre"), jsoS.getString("direccion"));
                	Evento e =  new Evento(jso.optInt("idevento"),jso.optString("titulo"), jso.optString("descripcion"), sdf.parse(fini), sdf.parse(ffin), jso.optString("tipoevento"), jso.optInt("plazas"), s);
                	eventos.add(e); 
				}
               } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("ServiceHandler", "Couldn't get any data from the url");
        }
    	
    	return eventos;
    }    
    
    public List<Evento> getProximosEventos() throws ParseException{
    	List<Evento> eventos = new ArrayList<Evento>();
    	
		// Making a request to url and getting response
		String url_evento = URLWB + "com.eventos.jpa.evento";
        String jsonStr = makeServiceCall(url_evento, ServiceHandler.GET);

        Log.d("Response: ", "> " + jsonStr);

        if (jsonStr != null) {
            try {
                JSONArray jsonObj = new JSONArray(jsonStr);
                for (int i = 0; i < jsonObj.length(); i++) {
                	JSONObject jso = jsonObj.getJSONObject(i);
                	Log.d("Response: ", "> " +jsonObj.toString());
                	
                	String fini = jso.getString("fechacomienzo");
                	String ffin = jso.getString("fechafinal");
                	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                	
                	JSONObject jsoS = jso.optJSONObject("idsede");
            		Sede s = new Sede(jsoS.getInt("idsede"), jsoS.getString("nombre"), jsoS.getString("direccion"));
                	Evento e =  new Evento(jso.optInt("idevento"),jso.optString("titulo"), jso.optString("descripcion"), sdf.parse(fini), sdf.parse(ffin), jso.optString("tipoevento"), jso.optInt("plazas"), s);
                	eventos.add(e); 
				}
               } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("ServiceHandler", "Couldn't get any data from the url");
        }
    	return eventos;
    }
    
	public List<Sede> getSedes() {
		// Making a request to url and getting response
		sedes = new ArrayList<Sede>();
		String url_sedes = URLWB + "com.eventos.jpa.sede";
        String jsonStr = makeServiceCall(url_sedes, ServiceHandler.GET);

        Log.d("Response: ", "> " + jsonStr);

        if (jsonStr != null) {
            try {
                JSONArray jsonObj = new JSONArray(jsonStr);
                for (int i = 0; i < jsonObj.length(); i++) {
                	JSONObject jso = jsonObj.getJSONObject(i);
                	Log.d("Response: ", "> " +jsonObj.toString());
                	 Sede s = new Sede(jso.optInt("idsede"),jso.optString("nombre"),jso.optString("direccion"));
                     sedes.add(s); 
				}
               } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("ServiceHandler", "Couldn't get any data from the url");
        }

        return sedes;
	}
}
