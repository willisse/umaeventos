package com.umaeventos.utils;

import java.util.List;

import com.umaeventos.R;
import com.umaeventos.modelo.Evento;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class EventosAdapter extends ArrayAdapter<Evento> {
	private Context context;
	private List<Evento> datos;

	/**
	 * Constructor del Adapter.
	 * 
	 * @param context
	 *            context de la Activity que hace uso del Adapter.
	 * @param datos
	 *            Datos que se desean visualizar en el ListView.
	 */
	public EventosAdapter(Context context, List<Evento> datos) {
		super(context, R.layout.layout_event,datos);
		// Guardamos los par?metros en variables de clase.
		this.context = context;
		this.datos = datos;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// En primer lugar "inflamos" una nueva vista, que ser? la que se
		// mostrar? en la celda del ListView.
		View item = LayoutInflater.from(context).inflate(R.layout.layout_event, null);

		TextView nombre = (TextView) item.findViewById(R.id.tvNombre);
		nombre.setText(datos.get(position).getTitulo());

		TextView tipo = (TextView) item.findViewById(R.id.tvSitio);
		tipo.setText(datos.get(position).getTipoevento() + " en " + datos.get(position).getSede().getNombre());
				
		TextView duracion = (TextView) item.findViewById(R.id.tvFecha);
		duracion.setText(datos.get(position).getFechacomienzo().toLocaleString() + " - " + String.valueOf(datos.get(position).getFechafinal().getHours()-datos.get(position).getFechacomienzo().getHours())+" horas");
		

		// Devolvemos la vista para que se muestre en el ListView.
		return item;
	}
}
