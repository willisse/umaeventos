package com.umaeventos.utils;


import java.util.List;

import com.umaeventos.R;
import com.umaeventos.modelo.Sede;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;



public class SedesAdapter extends ArrayAdapter<Sede> {

	 private Context context;
	 List<Sede> datos;
	 
	 public SedesAdapter(Context context, List<Sede> datos) {
         super(context, R.layout.layout_sede, datos);
         this.datos = datos;
         this.context = context;
     }

     public View getView(int position, View convertView, ViewGroup parent) {
		View item = LayoutInflater.from(context).inflate(
		R.layout.layout_sede, null);
	     
	    TextView lblNombreSede = (TextView)item.findViewById(R.id.LblNombreSede);
	    lblNombreSede.setText(datos.get(position).getNombre());
	
	    TextView lblDirSede = (TextView)item.findViewById(R.id.LblDirSede);
	    lblDirSede.setText(datos.get(position).getDireccion());

	     return(item);
	 }
}
